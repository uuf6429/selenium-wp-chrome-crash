<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/bin/common.php';

use Symfony\Component\Process\Process;

$run = function ($cmd, $ignoreFailure = false) {
    (new Process($cmd, __DIR__))
        ->setTimeout(null)
        ->{$ignoreFailure ? 'run' : 'mustRun'}(function ($type, $data) {
            fwrite($type === Process::STDOUT ? STDOUT : STDERR, $data);
        });
};
$app = new ConsoleApp();

try {
    $run('docker-compose up -d');

    touch('./output/wp-debug.log');

    $run([
        'php',
        'bin/wait4services.php',
        sprintf(
            'pdo-mysql://%s:%s@%s:%s/%s',
            $app->getEnv('WP_DB_USER'),
            $app->getEnv('WP_DB_PASS'),
            $app->getEnv('WP_DB_EXTERNAL_HOST'),
            $app->getEnv('WP_DB_PORT'),
            $app->getEnv('WP_DB_NAME')
        ),
        sprintf('http://%s/', $app->getEnv('WP_EXTERNAL_HOST')),
        $app->getEnv('WD_URL')
    ]);

    $run('php bin/install-wordpress.php');

    copy(__DIR__ . '/behat.yml.dist', __DIR__ . '/behat.yml');

    $run('php bin/replace-env-vars.php behat.yml');

    $run([
        'php',
        'vendor/behat/behat/bin/behat',
        '--config=behat.yml',
        '--no-interaction',
        '--colors',
        '--verbose',
    ]);
} finally {
    $run('docker-compose logs --no-color -t > ./output/docker-compose.log', true);
    $run('docker-compose rm -f -s -v', true);
}
