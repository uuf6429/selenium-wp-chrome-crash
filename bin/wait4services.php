<?php

require __DIR__ . '/common.php';

/**
 * CLI tool that replaces placeholders in the target file with values from environment variables.
 */
abstract class Adapter
{
    /**
     * @return string
     */
    abstract public function getName(): string;

    /**
     * @return string[]
     */
    abstract public function getExamples(): array;

    /**
     * @param string $dsn
     *
     * @return bool
     */
    abstract public function supports($dsn): bool;

    /**
     * @param string $dsn
     *
     * @return callable
     */
    abstract public function createCheckFunc($dsn): callable;
}

class PDOAdapter extends Adapter
{
    private $drivers;

    public function __construct()
    {
        $this->drivers = class_exists(\PDO::class) ? \PDO::getAvailableDrivers() : [];
    }

    public function getName(): string
    {
        return 'PDO';
    }

    public function getExamples(): array
    {
        return array_map(
            function ($driver) {
                return "pdo-$driver://user:pass@host/database";
            },
            $this->drivers
        );
    }

    public function supports($dsn): bool
    {
        $scheme = explode('-', parse_url($dsn, PHP_URL_SCHEME), 2) + [''];

        return $scheme[0] === 'pdo' && in_array($scheme[1], $this->drivers, true);
    }

    public function createCheckFunc($dsn): callable
    {
        $parts = parse_url($dsn);
        $pdoCs = sprintf(
            '%s:host=%s%s%s',
            explode('-', $parts['scheme'], 2)[1],
            $parts['host'],
            isset($parts['port']) ? ";port=${parts['port']}" : '',
            ($db = trim($parts['path'], '/')) ? ";dbname=$db" : ''
        );
        $user = $parts['user'] ?? null;
        $pass = $parts['pass'] ?? null;

        return function () use ($pdoCs, $user, $pass) {
            return (bool)@new \PDO($pdoCs, $user, $pass, [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION]);
        };
    }
}

class URLAdapter extends Adapter
{
    public function getName(): string
    {
        return 'Web';
    }

    public function getExamples(): array
    {
        return [
            'http://user:pass@host/test?a=b',
            'https://test.com/a/dir/huh.asp',
        ];
    }

    public function supports($dsn): bool
    {
        return in_array(parse_url($dsn, PHP_URL_SCHEME), ['http', 'https']);
    }

    public function createCheckFunc($dsn): callable
    {
        return function () use ($dsn) {
            $ch = curl_init();

            try {
                curl_setopt($ch, CURLOPT_URL, $dsn);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko)');
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                $output = curl_exec($ch);

                if (!is_string($output)) {
                    throw new \RuntimeException('Invalid response.');
                }

                $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                if ($code >= 400) {
                    throw new \RuntimeException("Error $code, json-encoded response: " . @json_encode($output));
                }

                return true;
            } finally {
                curl_close($ch);
            }
        };
    }
}

/**
 * @param string $name
 * @param mixed $default
 *
 * @return true|null|string
 */
function getArgvOpt($name, $default = null)
{
    global $argv;

    if (($pos = array_search($name, $argv, true)) !== false) {
        unset($argv[$pos]);

        return true;
    }

    $keyLen = strlen($name) + 1;
    foreach ($argv as $opt) {
        if (substr($opt, 0, $keyLen) === "$name=") {
            return substr($opt, $keyLen);
        }
    }

    return $default;
}

/** @var \Adapter[] $adapters */
$adapters = [new PDOAdapter(), new URLAdapter()];

$defaultTimeout = '+5 minutes';
$defaultDelay = 1.0;

$app = new ConsoleApp();
$app->setUsageSpec('php ' . basename(__FILE__) . ' [options] DSN...')
    ->setUsageDesc('Waits until all the specified service DSNs are reachable.')
    ->addHelpSectionDict(
        'Options',
        function () use ($app, $defaultTimeout, $defaultDelay) {
            return [
                '--help | -h' => 'Shows this help content.',
                '--timeout=TIMEOUT' => "Set the maximum time to wait (value can be anything supported by PHP's `strtotime()`). Default is {$app->YLW}$defaultTimeout{$app->RST}.",
                '--delay=DELAY' => "Set the delay between check runs in seconds (fractions also supported, eg; 0.5 = half a second). Default is {$app->YLW}$defaultDelay{$app->RST}.",
            ];
        }
    )
    ->addHelpSection(
        'Supported Adapters',
        function () use ($app, $adapters) {
            $table = [];
            $title = ['Adapter', 'Examples'];
            $colMax = strlen($title[0]);
            $result = [];
            foreach ($adapters as $adapter) {
                $colMax = max($colMax, strlen($adapter->getName()));
                $table[] = null;
                foreach ($adapter->getExamples() as $i => $example) {
                    $table[] = [($i === 0) ? $adapter->getName() : '', $example];
                }
            }
            $result[] = sprintf('%s | %s', str_pad($title[0], $colMax), $title[1]);
            foreach ($table as $row) {
                if ($row === null) {
                    $result[] = sprintf('%s-+-%s', str_repeat('-', $colMax), str_repeat('-', 40));
                } else {
                    $result[] = sprintf("{$app->GRN}%s{$app->RST} | {$row[1]}", str_pad($row[0], $colMax));
                }
            }

            return $result;
        }
    )
    ->setMainFunc(function () use ($app, $defaultTimeout, $defaultDelay, $adapters) {
        $app->ensureCliInvocation();

        global $argv, $argc;

        // handle blank arguments / help option
        if ($argc <= 1 || getArgvOpt('--help') || getArgvOpt('-h')) {
            throw new \NoOpException('No operation.');
        }

        // handle options
        $timeout = strtotime(getArgvOpt('--timeout', $defaultTimeout));
        $delay = getArgvOpt('--delay', $defaultDelay) * 1000000;

        // check DSN support
        $dsnAdapters = [];
        foreach (array_slice($argv, 1) as $dsn) {
            foreach ($adapters as $adapter) {
                if ($adapter->supports($dsn)) {
                    $dsnAdapters[$dsn] = $adapter->createCheckFunc($dsn);
                    break;
                }
            }
            if (!isset($dsnAdapters[$dsn])) {
                throw new \RuntimeException("DSN $dsn is not supported by any adapter.");
            }
        }

        // check DSNs
        $dsnErrors = [];
        $dsnCount = count($dsnAdapters);
        echo "Waiting for {$app->GRN}{$dsnCount}{$app->RST} services...\n";
        while ($dsnAdapters) {
            foreach ($dsnAdapters as $dsn => $checkFunc) {
                $success = false;
                try {
                    if (!($success = $checkFunc())) {
                        throw new \RuntimeException('Unknown reason.');
                    }
                } catch (\Exception $ex) {
                    $dsnErrors[$dsn] = "  {$dsn}:"
                        . str_replace("\n", "\n  | ", "\n" . trim($ex))
                        . "\n";
                }
                if ($success) {
                    unset($dsnAdapters[$dsn], $dsnErrors[$dsn]);
                    echo "\n  {$app->GRN}ONLINE {$app->RST} {$dsn}\n";
                } else {
                    echo '.';
                }
            }

            if ($dsnAdapters) {
                usleep($delay);
            }

            if ($timeout < time()) {
                echo "\n{$app->RED}Gave up waiting for the following services:\n" . implode("\n", $dsnErrors) . "{$app->RST}\n";
                exit(1);
            }
        }
    })
    ->run();
