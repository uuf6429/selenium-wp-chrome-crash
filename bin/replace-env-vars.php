<?php

require __DIR__ . '/common.php';

$app = new ConsoleApp();
$app->setUsageSpec('php ' . basename(__FILE__) . ' FILE')
    ->setUsageDesc([
        'Replaces double curly bracket variables in file with values of environment variables.',
        "For example, a file with {$app->YLW}Hello {{OS}}!{$app->GRN} will change to {$app->YLW}Hello Windows_NT!{$app->GRN} on Windows.",
    ])
    ->setMainFunc(function () use ($app) {
        $app->ensureCliInvocation();
        $app->ensureCliMinArgs(1);
        $app->ensureCliMaxArgs(1);

        global $argv;
        $file = $argv[1];
        file_put_contents(
            $file,
            preg_replace_callback(
                '/{{([a-zA-Z_][\\w]*)}}/',
                function ($matches) use ($app) {
                    if (($value = getenv($matches[1])) === false) {
                        $app->showWarning("Environment variable {$app->YLW}{$matches[1]}{$app->RST} is not set and won't be replaced.");

                        return $matches[0];
                    }

                    return $value;
                },
                file_get_contents($file)
            )
        );
    })
    ->run();
