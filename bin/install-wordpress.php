<?php

require __DIR__ . '/common.php';

$app = new ConsoleApp();
$app->setUsageSpec('php ' . basename(__FILE__))
    ->setUsageDesc('Runs the last step of the WordPress installer and sets the correct site URL.')
    ->addHelpSectionDict(
        'Environment Variables',
        function () use ($app) {
            static $repeat = '                 "';

            return [
                'WP_EXTERNAL_HOST' => "Look at {$app->YLW}.gitlab-ci.yml{$app->RST} for more info.",
                'WP_INTERNAL_HOST' => $repeat,
                'WP_USER' => $repeat,
                'WP_PASS' => $repeat,
                'WP_DB_EXTERNAL_HOST' => $repeat,
                'WP_DB_PORT' => $repeat,
                'WP_DB_NAME' => $repeat,
                'WP_DB_USER' => $repeat,
                'WP_DB_PASS' => $repeat,
                'WP_DB_PRFX' => $repeat,
            ];
        }
    )
    ->setMainFunc(function () use ($app) {
        $app->ensureCliInvocation();
        $app->ensureCliMinArgs(0);
        $app->ensureCliMaxArgs(0);

        $url = sprintf('http://%s/wp-admin/install.php?step=2', getenv('WP_EXTERNAL_HOST'));
        $arg = http_build_query([
            'weblog_title' => 'WSAL QATA',
            'user_name' => getenv('WP_USER'),
            'admin_email' => 'test@example.com',
            'admin_password' => getenv('WP_PASS'),
            'admin_password2' => getenv('WP_PASS'),
            'pw_weak' => '1',
        ]);

        echo "> {$app->YLW}POST {$url}{$app->RST}\n";
        echo "> {$app->YLW}$arg{$app->RST}\n";

        $resp = file_get_contents(
            $url,
            false,
            stream_context_create(
                [
                    'http' => [
                        'method' => 'POST',
                        'header' => 'Content-type: application/x-www-form-urlencoded',
                        'content' => $arg
                    ],
                ]
            )
        );
        $resp = preg_replace(['/ +/', '/\t/', '/\n+/'], [' ', '    ', "\n"], strip_tags(preg_replace('#<script(.*?)>(.*?)<' . '/script>#is', '', $resp)));

        echo "< {$app->YLW}$resp{$app->RST}\n";

        $pdo = new \PDO(
            sprintf(
                'mysql:host=%s;port=%s;dbname=%s',
                getenv('WP_DB_EXTERNAL_HOST'),
                getenv('WP_DB_PORT'),
                getenv('WP_DB_NAME')
            ),
            getenv('WP_DB_USER') ?? null,
            getenv('WP_DB_PASS') ?? null,
            [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION]
        );
        $query = sprintf(
            "UPDATE `%soptions` SET `option_value` = 'http://%s' WHERE `option_name` IN ('siteurl', 'home');",
            getenv('WP_DB_PRFX') ?? 'wp_',
            getenv('WP_INTERNAL_HOST')
        );

        echo "> {$app->YLW}{$query}{$app->RST}\n";

        $count = $pdo->exec($query);

        echo "< {$app->YLW}{$count} rows updated.{$app->RST}\n";
    })
    ->run();
