<?php

class NoOpException extends Exception
{
}

class InternalException extends Exception
{
}

/**
 * @property-read string $RED
 * @property-read string $GRN
 * @property-read string $YLW
 * @property-read string $RST
 * @property-read string $GRN_B
 * @property callable $mainFunc
 * @property null|callable $finalFunc
 * @property string $usageSpec
 * @property string $usageDesc
 * @property array $helpSections
 * @method $this setMainFile(string $newValue)
 * @method $this setMainFunc(callable $newValue)
 * @method $this setFinalFunc(callable $newValue)
 * @method $this setUsageSpec(string $newValue)
 * @method $this setUsageDesc(string | string[] $newValue)
 */
class ConsoleApp
{
    protected static $readonlyProps = [
        'RED' => "\e[31m",
        'GRN' => "\e[32m",
        'YLW' => "\e[33m",
        'RST' => "\e[0m",
        'GRN_B' => "\e[32;1m",
    ];
    protected static $requiredProps = [
        'mainFunc',
        'usageSpec',
        'usageDesc',
    ];
    protected static $optionalProps = [
        'finalFunc',
        'helpSections',
    ];

    private $properties;

    private $environment;

    public function __construct()
    {
        $this->properties = array_merge(
            static::$readonlyProps,
            array_fill_keys(static::$requiredProps, null),
            array_fill_keys(static::$optionalProps, null)
        );

        set_error_handler(
            function ($errNo, $errStr, $errFile = 'unknown', $errLine = 0) {
                throw new ErrorException($errStr, 0, $errNo, $errFile, $errLine);
            }
        );

        $this->environment = getenv();
    }

    public function &__get($name)
    {
        return $this->properties[$name];
    }

    public function __set($name, $value)
    {
        if (array_key_exists($name, static::$readonlyProps)) {
            throw new \RuntimeException("Property \"$name\" is readonly.");
        }

        $this->properties[$name] = $value;
    }

    public function __isset($name)
    {
        return array_key_exists($name, $this->properties);
    }

    public function __call($name, $arguments): self
    {
        $propName = strtolower(sprintf('%s     ', $name)[3]) . substr($name, 4);

        if (strpos($name, 'set') !== 0) {
            throw new \RuntimeException("Call to undefined method \"{$name}\".");
        }

        if (count($arguments) !== 1) {
            throw new \RuntimeException("Invalid number of arguments to \"{$name}\" (only one was expected).");
        }

        $this->$propName = $arguments[0];

        return $this;
    }

    public function run(): void
    {
        $exitCode = 0;

        try {
            $emptyProperties = array_filter(
                $this->properties,
                function ($value, $key) {
                    return $value === null && in_array($key, static::$requiredProps, true);
                },
                ARRAY_FILTER_USE_BOTH
            );

            if ($emptyProperties) {
                throw new \RuntimeException('The following properties were not configured: ' . implode(', ', array_keys($emptyProperties)));
            }

            $exitCode = ($this->mainFunc)();
        } catch (\Throwable $ex) {
            if (!$ex instanceof \NoOpException) {
                echo "\n  {$this->RED}{$ex->getMessage()}{$this->RST}\n\n";
            }

            $exitCode = $exitCode ?: 1;
        } finally {
            if ($this->finalFunc) {
                ($this->finalFunc)();
            }

            if ($exitCode && isset($ex) && !$ex instanceof InternalException) {
                echo "\n";
                $this->showHelp();
            }

            if (!isset($ex)) {
                echo "{$this->GRN}OK{$this->RST}\n";
            }
        }

        exit($exitCode);
    }

    public function showHelp(): void
    {
        $usageDesc = is_array($this->usageDesc) ? implode("\n", $this->usageDesc) : $this->usageDesc;

        echo "{$this->YLW}Usage:{$this->RST}\n";
        echo "  {$this->usageSpec}\n";
        echo "\n";
        echo "{$this->GRN}{$usageDesc}{$this->RST}\n\n";

        foreach ((array)$this->helpSections as [$name, $generator]) {
            $content = $generator();
            if (is_array($content)) {
                $content = implode("\n", $content);
            }
            $content = str_replace("\n", "\n  ", "\n" . trim($content) . "\n");
            echo "{$this->YLW}{$name}:{$this->RST}{$content}\n";
        }
    }

    public function showWarning(string $message): void
    {
        echo "{$this->YLW}Warning:{$this->RST} {$message}.\n";
    }

    public function addHelpSection(string $name, callable $generator): self
    {
        $this->helpSections[] = [$name, $generator];

        return $this;
    }

    public function addHelpSectionDict(string $name, callable $generator): self
    {
        $this->helpSections[] = [
            $name,
            function () use ($generator) {
                $hash = $generator();
                $keys = array_keys($hash);
                $values = array_values($hash);
                $longest = max(array_map('\strlen', $keys)) + 3;

                return array_map(
                    function ($key, $value) use ($longest) {
                        return sprintf("{$this->GRN}%s{$this->RST} {$value}", str_pad($key, $longest));
                    },
                    $keys,
                    $values
                );
            }
        ];

        return $this;
    }

    public function ensureCliInvocation(): void
    {
        global $argv;

        if ($argv === null) {
            throw new \LogicException('This program is only available for use with CLI.');
        }
    }

    public function ensureCliMinArgs(int $count): void
    {
        global $argc;

        if ($argc < $count + 1) {
            throw new \LogicException('File to be modified was expected as first argument.');
        }
    }

    public function ensureCliMaxArgs(int $count): void
    {
        global $argc;

        if ($argc > $count + 1) {
            throw new \LogicException('Too many arguments passed to program.');
        }
    }

    public function popOption(string $name): bool
    {
        global $argc, $argv;

        if (($pos = array_search($name, $argv, true)) === false) {
            return false;
        }

        $argc--;
        unset($argv[$pos]);
        $argv = array_values($argv);

        return true;
    }

    public function popParameter(string $name, $default = null)
    {
        global $argc, $argv;

        if (($pos = array_search($name, $argv, true)) === false) {
            return $default;
        }

        $argc--;
        unset($argv[$pos]);
        $argv = array_values($argv);

        if (!array_key_exists($pos, $argv)) {
            return $default;
        }

        $argc--;
        $value = $argv[$pos];
        unset($argv[$pos]);
        $argv = array_values($argv);

        return $value;
    }

    /**
     * @param string|null $key
     * @return false|string
     */
    public function getEnv(string $key = null)
    {
        if ($key === null) {
            return $this->environment;
        }

        return array_key_exists($key, $this->environment) ? $this->environment[$key] : false;
    }

    /**
     * @param string $keyVal
     */
    public function putEnv(string $keyVal): void
    {
        [$key, $val] = explode('=', $keyVal, 2);

        $this->environment[$key] = $val;
    }

    /**
     * @param string|string[] $cmd
     * @param bool $ignoreFailure
     * @param null|string $currentWorkingDirectory
     * @return int
     * @throws InternalException
     */
    public function runCmd($cmd, bool $ignoreFailure = false, ?string $currentWorkingDirectory = null): int
    {
        global $app;

        if (is_array($cmd)) {
            $exe = array_shift($cmd);
            $cmd = $exe . ' ' . implode(' ', array_map('escapeshellarg', $cmd));
        }

        $desc = [
            0 => ['file', 'php://stdin', 'r'],
            1 => ['file', 'php://stdout', 'w'],
            2 => ['file', 'php://stderr', 'w'],
        ];

        echo "{$app->GRN_B}\$ {$cmd}{$app->RST}\n";

        $process = proc_open($cmd, $desc, $pipes, $currentWorkingDirectory, $app->getEnv());
        $exitCode = proc_close($process);

        if (!$ignoreFailure && $exitCode) {
            throw new InternalException("Process quit with exit code $exitCode.");
        }

        return $exitCode;
    }

    /**
     * @param string $script
     * @param string[] $args
     * @param bool $ignoreFailure
     * @param null|string $currentWorkingDirectory
     * @return int
     * @throws InternalException
     */
    public function runPhpScript(
        string $script,
        array $args = [],
        bool $ignoreFailure = false,
        ?string $currentWorkingDirectory = null
    ): int
    {
        static $xdebugEnabledFn = 'xdebug_is_enabled';

        $cmd = ['php'];

        if (function_exists($xdebugEnabledFn) && $xdebugEnabledFn()) {
            if ($dbg = $this->getEnv('XDEBUG_PATH')) {
                $cmd[] = escapeshellarg("-dzend_extension={$dbg}");
            }
            foreach (
                [
                    'xdebug.remote_enable',
                    'xdebug.remote_mode',
                    'xdebug.remote_port',
                    'xdebug.remote_host',
                ] as $key
            ) {
                $cmd[] = escapeshellarg("-d{$key}=" . ini_get($key));
            }
        }

        $cmd[] = escapeshellarg($script);

        if (count($args)) {
            $cmd[] = implode(' ', array_map('escapeshellarg', $args));
        }

        return $this->runCmd(implode(' ', $cmd), $ignoreFailure, $currentWorkingDirectory);
    }
}
