<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Behat\Context\Environment\InitializedContextEnvironment;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Mink\Driver\Selenium2Driver;
use WebDriver\Exception\NoAlertOpenError;
use Behat\Mink\Element\NodeElement;
use Behat\Mink\Exception\ElementNotFoundException;
use Behat\Mink\Exception\ExpectationException;

class FeatureContext implements Context
{
    /**
     * @var MinkContext
     */
    private $minkContext;

    /**
     * @BeforeScenario
     *
     * @param BeforeScenarioScope $scope
     */
    public function gatherContexts(BeforeScenarioScope $scope): void
    {
        $environment = $scope->getEnvironment();

        if (!$environment instanceof InitializedContextEnvironment) {
            throw new \RuntimeException('Environment is not initialized.');
        }

        $this->minkContext = $environment->getContext(MinkContext::class);
    }

    /**
     * @Given /^I log in as "(?P<user>[^"]*)" with password "(?P<pass>[^"]*)"$/
     *
     * @param string $user
     * @param string $pass
     */
    public function iLogInAs(string $user, string $pass): void
    {
        $this->minkContext->visit('wp-admin');
        usleep(500000); // 0.5s
        $this->waitForField('user_login');
        $this->minkContext->fillField('user_login', $user);
        $this->minkContext->fillField('user_pass', $pass);
        $this->minkContext->pressButton('Log In');
    }

    /**
     * @Given /^I (?P<action>accept|dismiss) the popup$/
     *
     * @param string $action
     */
    public function iClickThePopup(string $action): void
    {
        $driver = $this->minkContext->getSession()->getDriver();

        if (!$driver instanceof Selenium2Driver) {
            throw new \RuntimeException('This step is supported only on Selenium2Driver sessions.');
        }

        $result = $this->minkContext->getSession()
            ->getPage()
            ->waitFor(60, function () use ($action, $driver) {
                try {
                    switch ($action) {
                        case 'accept':
                            $driver->getWebDriverSession()->accept_alert();
                            break;
                        case 'dismiss':
                            $driver->getWebDriverSession()->dismiss_alert();
                            break;
                    }
                    return true;
                } catch (NoAlertOpenError $e) {
                    return false;
                }
            });

        if (!$result) {
            throw new \RuntimeException('Gave up waiting for popup.');
        }
    }

    /**
     * Wait until a field is exists and is visible.
     * Example: When I wait for "uploadComplete"
     *
     * @When /^(?:|I )wait for field "(?P<field>(?:[^"]|\\")*)"$/
     *
     * @param string $field
     */
    public function waitForField($field): void
    {
        $result = $this->minkContext
            ->getSession()
            ->getPage()
            ->waitFor(60, function () use ($field) {
                $field = $this->minkContext->getSession()->getPage()->findField($field);

                return $field && $field->isVisible();
            });

        if (!$result) {
            throw new \RuntimeException('Gave up waiting for "' . $field . '" field to show up.');
        }
    }

    /**
     * @Given /^I wait for "([^"]*)"$/
     *
     * @param string $duration
     */
    public function iWaitFor(string $duration): void
    {
        sleep(abs(strtotime($duration) - time()));
    }

    /**
     * @Given /^I hover over the (?P<index>\d+(st|nd|rd|th))? "(?P<text>[^"]*)" text$/
     *
     * @param string $text
     * @param null|string $index
     */
    public function iHoverOver(string $text, ?string $index = null): void
    {
        $actualIndex = $index === null ? null : (int)substr($index, 0, -2);
        $this->findByTextAndIndex($text, $actualIndex)->mouseOver();
    }

    /**
     * @Given /^I submit the (?P<index>\d+(st|nd|rd|th)) form$/
     *
     * @param string $index
     *
     * @throws ElementNotFoundException
     * @throws ExpectationException
     */
    public function iSubmitTheNthForm($index): void
    {
        $this->findElement('xpath', '//form[' . (int)substr($index, 0, -2) . ']')->submit();
    }

    /**
     * Wait until some text shows up.
     * Example: When I wait for "Success" text
     *
     * @When /^(?:|I )wait for text "(?P<text>(?:[^"]|\\")*)"$/
     *
     * @param string $text
     */
    public function waitForText($text): void
    {
        $result = $this->minkContext
            ->getSession()
            ->getPage()
            ->waitFor(300, function () use ($text) {
                /** @see \Behat\Mink\Element\DocumentElement::hasContent */
                $contentElements = $this->minkContext
                    ->getSession()
                    ->getPage()
                    ->find('named', ['content', $text]);

                return $contentElements !== null;
            });

        if (!$result) {
            throw new \RuntimeException('Gave up waiting for "' . $text . '" to show up.');
        }
    }

    /**
     * @When /^(?:|I )wait for the (?P<index>\d+(st|nd|rd|th))? "(?P<text>(?:[^"]|\\")*)" text$/
     *
     * @param string $text
     * @param null|string $index
     */
    public function waitForTheNthText($text, ?string $index = null): void
    {
        $actualIndex = $index === null ? null : (int)substr($index, 0, -2);

        $result = $this->minkContext
            ->getSession()
            ->getPage()
            ->waitFor(300, function () use ($text, $actualIndex) {
                try {
                    return $this->findByTextAndIndex($text, $actualIndex);
                } catch (\RuntimeException $ex) {
                    return false;
                }
            });

        if (!$result) {
            throw new \RuntimeException('Gave up waiting for the ' . $index . ' "' . $text . '" to show up.');
        }
    }

    /**
     * @param string $selectorType
     * @param string|array $selector
     *
     * @return NodeElement
     *
     * @throws ElementNotFoundException
     * @throws ExpectationException
     */
    private function findElement($selectorType, $selector): NodeElement
    {
        $elements = $this->minkContext
            ->getSession()
            ->getPage()
            ->findAll($selectorType, $selector);

        switch (\count($elements)) {
            case 0:
                throw new ElementNotFoundException($this->minkContext->getSession()->getDriver(), 'element', $selectorType, $selector);
            case 1:
                return $elements[0];
            default:
                throw new ExpectationException(
                    sprintf('Expected to find one element for %s selector "%s", but found %d', $selectorType, $selector, \count($elements)),
                    $this->minkContext->getSession()->getDriver()
                );
        }
    }

    /**
     * @param string $text
     * @param int|null $index
     *
     * @return NodeElement
     */
    private function findByTextAndIndex(string $text, ?int $index): NodeElement
    {
        if (strpos('\'', $text) !== false && strpos('"', $text) !== false) {
            throw new \RuntimeException('Text cannot contain double and single quotes at the same time since quotes cannot be escape.');
        }

        $quote = strpos('"', $text) === false ? '\'' : '"';

        $xpath = "//*[normalize-space(text())=$quote$text$quote]";
        if ($index !== null) {
            $xpath = sprintf('(%s)[%s]', $xpath, $index);
        }

        /** @var NodeElement[] $elements */
        $elements = $this->minkContext
            ->getSession()
            ->getPage()
            ->findAll('xpath', $xpath);

        if (\count($elements) !== 1) {
            throw new \RuntimeException(
                $index === null
                    ? sprintf('Expected to find one element with text "%s", but found %d', $text, \count($elements))
                    : sprintf('The %s element with text "%s" was not found', $this->toOrdinal($index), $text)
            );
        }

        return $elements[0];
    }

    /**
     * Converts integers (eg; 56) to ordinals (eg; 56th).
     *
     * @param int $number
     * @return string
     */
    private function toOrdinal(int $number): string
    {
        static $suffix = [1 => 'st', 2 => 'nd', 3 => 'rd'];

        return $number . ((($number % 100) >= 11 && ($number % 100) <= 13) || !isset($suffix[$number % 10]) ? 'th' : $suffix[$number % 10]);
    }
}
