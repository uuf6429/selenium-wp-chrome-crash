@wp
Feature: Theme management

  Scenario: Install and delete theme (serena)
    Given I log in as "wpuser" with password "wppass"
    When I am on "wp-admin/theme-install.php?search=serena"
      And I wait for text "Serena"
      And I hover over the 1st "Serena" text
      And I wait for text "Details & Preview"
      And I follow "Install"
      And I wait for the 1st "Activate" text
      And I am on "wp-admin/themes.php?theme=serena"
      And I follow "Delete"
      And I accept the popup
      And I wait for "2 seconds"
    Then I should see "success!"
